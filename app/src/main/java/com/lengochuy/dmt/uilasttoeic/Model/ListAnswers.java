package com.lengochuy.dmt.uilasttoeic.Model;

public class ListAnswers {
    private String label;
    private String content;
    private boolean isRight;

    public ListAnswers(String label, String content, boolean isRight) {
        this.label = label;
        this.content = content;
        this.isRight = isRight;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public boolean isRight() {
        return isRight;
    }

    public void setRight(boolean right) {
        isRight = right;
    }

    @Override
    public String toString() {
        return "{" +
                "label='" + label + '\'' +
                ", content='" + content + '\'' +
                ", isRight=" + isRight +
                '}';
    }
}
