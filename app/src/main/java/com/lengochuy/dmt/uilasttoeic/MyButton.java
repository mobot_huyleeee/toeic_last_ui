package com.lengochuy.dmt.uilasttoeic;

import android.content.Context;
import android.util.AttributeSet;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatButton;

public class MyButton extends AppCompatButton {

    public MyButton(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    private static final int[] STATE_FRIED = {R.attr.state_fried};
    private static final int[] STATE_BAKED = {R.attr.state_baked};

    private boolean mIsFried = false;
    private boolean mIsBaked = false;

    public void setFried(boolean isFried) {mIsFried = isFried;}
    public void setBaked(boolean isBaked) {mIsBaked = isBaked;}

    @Override
    protected int[] onCreateDrawableState(int extraSpace) {
        final int[] drawableState = super.onCreateDrawableState(extraSpace + 2);
        if (mIsFried) {
            mergeDrawableStates(drawableState, STATE_FRIED);
        }
        if (mIsBaked) {
            mergeDrawableStates(drawableState, STATE_BAKED);
        }
        return drawableState;
    }
}
