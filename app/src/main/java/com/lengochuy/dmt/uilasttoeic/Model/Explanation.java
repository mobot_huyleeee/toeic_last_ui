package com.lengochuy.dmt.uilasttoeic.Model;

public class Explanation {
    private String content;

    public Explanation(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return "{" +
                "content='" + content + '\'' +
                '}';
    }
}
